import datetime
from django.contrib.auth.models import AbstractUser
from django.db import models

class Account(AbstractUser):
    dob = models.DateField(default=datetime.date.today)

    def __str__(self):
        return self.username + ' ' + dob
