from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from .models import Account

class UserSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(
        required=True,
        validators=[UniqueValidator(queryset=Account.objects.all())]
    )

    username = serializers.CharField(
        max_length=32,
        required=True,
        validators=[UniqueValidator(queryset=Account.objects.all())]
    )

    password = serializers.CharField(
        min_length=8,
        required=True,
        write_only=True
    )

    def create(self, validated_data):
        user = Account.objects.create_user(
            username=validated_data['username'],
            email=validated_data['email'],
            password=validated_data['password']
        )
        return user


    class Meta:
        model = Account
        fields = ('id', 'username', 'email', 'password', 'dob')
