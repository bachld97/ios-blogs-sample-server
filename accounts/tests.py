from django.urls import reverse
from rest_framework.test import APITestCase
from rest_framework import status
from rest_framework.authtoken.models import Token
from .models import Account


class SignupTests(APITestCase):
    def setUp(self):
        self.test_user = Account.objects.create_user(
            'testuser', 'test@test.com', 'testpassword'
        )
        self.signup_url = reverse('signup')


    def test_create_user_success(self):
        data = {
            'username': 'newuser',
            'email': 'newuser@test.com',
            'password': 'newpassword'
        }
        response = self.client.post(self.signup_url, data, format='json')
        user = Account.objects.latest('id')

        self.assertEqual(Account.objects.count(), 2)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        resData = response.data
        self.assertEqual(resData['username'], 'newuser')
        self.assertEqual(resData['email'], 'newuser@test.com')
        self.assertFalse('password' in response.data)
        token = Token.objects.get(user=user)
        self.assertEqual(response.data['token'], token.key)
        self.assertFalse('id' in response.data)


    def test_create_user_with_short_password(self):
        data = {
            'username': 'newuser',
            'email': 'newuser@test.com',
            'password': 'short'
        }
        response = self.client.post(self.signup_url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Account.objects.count(), 1)
        self.assertEqual(len(response.data['password']), 1)


    def test_create_user_with_no_password(self):
        data = {
            'username': 'newuser',
            'email': 'newuser@test.com',
            'password': ''
        }
        response = self.client.post(self.signup_url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Account.objects.count(), 1)
        self.assertEqual(len(response.data['password']), 1)


    def test_create_user_with_long_username(self):
        data = {
            'username': 'longusername' * 20,
            'email': 'newuser@test.com',
            'password': 'newpassword'
        }
        response = self.client.post(self.signup_url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Account.objects.count(), 1)
        self.assertEqual(len(response.data['username']), 1)


    def test_create_user_with_no_username(self):
        data = {
            'username': '',
            'email': 'newuser@test.com',
            'password': 'newpassword'
        }
        response = self.client.post(self.signup_url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Account.objects.count(), 1)
        self.assertEqual(len(response.data['username']), 1)


    def test_create_user_with_existing_username(self):
        data = {
            'username': 'testuser',
            'email': 'newuser@test.com',
            'password': 'newpassword'
        }
        response = self.client.post(self.signup_url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Account.objects.count(), 1)
        self.assertEqual(len(response.data['username']), 1)


    def test_create_user_with_existing_email(self):
        data = {
            'username': 'newuser',
            'email': 'test@test.com',
            'password': 'newpassword'
        }
        response = self.client.post(self.signup_url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Account.objects.count(), 1)
        self.assertEqual(len(response.data['email']), 1)


    def test_create_user_with_invalid_email(self):
        data = {
            'username': 'newuser',
            'email': 'invalidemail',
            'password': 'newpassword'
        }
        response = self.client.post(self.signup_url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Account.objects.count(), 1)
        self.assertEqual(len(response.data['email']), 1)


    def test_create_user_with_no_email(self):
        data = {
            'username': 'newuser',
            'email': '',
            'password': 'newpassword'
        }
        response = self.client.post(self.signup_url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Account.objects.count(), 1)
        self.assertEqual(len(response.data['email']), 1)


class LoginTests(APITestCase):
    def setUp(self):
        self.test_user = Account.objects.create_user(
            'testuser', 'test@test.com', 'testpassword'
        )
        self.login_url = reverse('login')


    def test_login_success(self):
        data = {
            'username': 'testuser',
            'password': 'testpassword',
            'email': 'test@test.com'
        }
        response = self.client.post(self.login_url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        resData = response.data 
        self.assertTrue('token' in resData)
        self.assertTrue('status' in resData)
        self.assertTrue('message' in resData)
        self.assertEqual(resData['user']['username'], 'testuser')
        self.assertEqual(resData['user']['dob'], 'fake')
        self.assertEqual(resData['user']['avatar'], 'fake')
