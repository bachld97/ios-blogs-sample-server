from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from django.contrib.auth import authenticate
from .serializers import UserSerializer

class UserSignup(APIView):
    def post(self, request, format='json'):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            user = serializer.save()
            if user:
                token = Token.objects.create(user=user)
                json = serializer.data
                json['token'] = token.key
                del(json['id'])
                return Response(json, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class UserLogin(APIView):
    def post(self, request, format='json'):
        username = request.data.get('username', None)
        password = request.data.get('password', None)

        if username is None or password is None:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        user = authenticate(request, username=username, password=password)
        response_data = {}
        if user is None:
            response_data = { 'status': 1, 'message': 'Invalid login'}
        else:
            token, created = Token.objects.get_or_create(user=user)

            response_data = {
                'status': 0,
                'message': 'Login successfully',
                'token': token.key,
                'user': { 'username': request.data['username'], 'avatar': 'fake', 'dob': 'fake' }
            }
        return Response(response_data, status=status.HTTP_200_OK)


