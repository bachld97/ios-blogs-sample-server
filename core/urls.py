from django.urls import path
from .views import FeedView, ImageUploadView

urlpatterns = [
    path('upload/', ImageUploadView.as_view(), name='upload_image')
]
