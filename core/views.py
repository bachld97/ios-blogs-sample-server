from rest_framework import status
from rest_framework.parsers import FileUploadParser
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from .serializers import ImageSerializer

class ImageUploadView(APIView):
    parser_class = (FileUploadParser,)

    def post(self, request, *args, **kwargs):
        img_serializer = ImageSerializer(data=request.data)
        if img_serializer.is_valid():
            img_serializer.save()
            return Response(
                img_serializer.data,
                status=status.HTTP_201_CREATED
            )
        else:
            return Response(
                img_serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )


class FeedView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        content = { 'posts': 'Fake'}
        return Response(content)
